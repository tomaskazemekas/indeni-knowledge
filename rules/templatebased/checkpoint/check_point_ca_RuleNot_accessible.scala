package com.indeni.server.rules.library.templatebased.checkpoint

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule
import com.indeni.time.TimeSpan

/**
  *
  */
case class check_point_ca_not_accessible(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "check_point_ca_not_accessible",
  howManyRepetitions = 2,
  ruleFriendlyName = "Check Point Firewalls: Certificate authority not accessible",
  ruleDescription = "If the certificate authority is not accessible to a firewall, VPN tunnels relying on certificates may fail.",
  metricName = "ca-accessible",
  applicableMetricTag = "name",
  alertItemsHeader = "Unreachable Certificate Authorities",
  alertDescription = "Some of the certificate authority servers which this device considers to be those to be used during authentication (for example - for VPN) are not accessible. The CA servers for which an issue has been found are listed below. If the connectivity issue remains for more than a few hours, some VPN tunnels may fail.\n\nThis alert was added per the request of Mart Khizner (Leumi Card).",
  baseRemediationText = "Identify why the device cannot initiate a connection with the listed servers.")()
