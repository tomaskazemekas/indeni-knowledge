package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{ConditionalRemediationSteps, NearingCapacityWithItemsTemplateRule}

/**
  *
  */
case class cross_vendor_tx_drop(context: RuleContext) extends NearingCapacityWithItemsTemplateRule(context,
  ruleName = "cross_vendor_tx_drop",
  ruleFriendlyName = "All Devices: TX packets dropped",
  ruleDescription = "Indeni tracks the number of packets that had issues and alerts if the ratio is too high.",
  usageMetricName = "network-interface-tx-dropped",
  limitMetricName = "network-interface-tx-packets",
  applicableMetricTag = "name",
  threshold = 0.5,
  minimumValueToAlert = 100.0, // We don't want to alert if the number of packets is really low
  alertDescription = "Some network interfaces and ports are experiencing a high drop rate. Review the ports below.",
  alertItemDescriptionFormat = "%.0f dropped packets identified out of a total of %.0f transmitted.",
  baseRemediationText = "Packet drops usually occur when the rate of packets received is higher than the device's ability to handle.",
  alertItemsHeader = "Affected Ports")(
  ConditionalRemediationSteps.OS_NXOS ->
    """|
      |1. Run the "show interface" command to review the interface counters and the bitrate. Consider to configure the "load-interval 30" interface sub command to improve the accuracy of the interface measurements. Check for traffic bursts and high traffic utilization.
      |2. Use the "show hardware rate-limit" NX-OS command (if supported) to determine if packets are being dropped because of a rate limit.
      |3. Execute the "show policy-map interface control-plane" NX-OS command to determine if packets are being dropped because of a QoS policy.
      |4. Use the "show hardware internal statistics rates" to determine if packets are being dropped by the hardware.
      |5. Run the "show hardware internal statistics pktflow all" NX-OS command to display per ASIC statistics, including packets into and out of the ASIC. This command helps to identify where packet loss is occurring.
    """.stripMargin
)
