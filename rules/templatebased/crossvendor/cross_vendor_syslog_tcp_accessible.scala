package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.StateDownTemplateRule

/**
  *
  */
case class cross_vendor_syslog_tcp_accessible(context: RuleContext) extends StateDownTemplateRule(context,
  ruleName = "cross_vendor_syslog_tcp_accessible",
  ruleFriendlyName = "All Devices: TCP syslog server is not reachable",
  ruleDescription = "indeni will alert if one of the Syslog servers configured for use over TCP is not responding.",
  metricName = "tcp-syslog-state",
  applicableMetricTag = "name",
  alertItemsHeader = "Unreachable Syslog Servers",
  alertDescription = "One of the configured syslog servers is not reachable from the device. This could result in lost messages which would result in traceability being severely impacted.",
  baseRemediationText = "Verify that the firewall is allowing the traffic and that the syslog service is running."
)(
)
