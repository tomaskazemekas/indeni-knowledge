package com.indeni.server.rules.library.templatebased.crossvendor

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.SnapshotComparisonTemplateRule

/**
  *
  */
case class cross_vendor_compare_model(context: RuleContext) extends SnapshotComparisonTemplateRule(context,
  ruleName = "cross_vendor_compare_model",
  ruleFriendlyName = "Clustered Devices: Model mismatch across cluster members",
  ruleDescription = "indeni will identify when two devices are part of a cluster and alert if the model of device in use is different.",
  metricName = "model",
  isArray = false,
  alertDescription = "The members of a cluster of devices must have the same device models in use.",
  baseRemediationText = "Replace one of the devices to match the other.")()
