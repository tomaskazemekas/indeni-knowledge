package com.indeni.server.rules.library.templatebased.f5

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.{NumericThresholdOnComplexMetricWithItemsTemplateRule, ThresholdDirection}

/**
  *
  */
case class f5_high_idle_timeout(context: RuleContext) extends NumericThresholdOnComplexMetricWithItemsTemplateRule(context,
  ruleName = "f5_high_idle_timeout",
  ruleFriendlyName = "F5 Devices: Virtual server using a TCP profile with a high idle timeout",
  ruleDescription = "Having very long TCP idle timeouts for virtual servers could make the load balancer keep too many connections open, which in turn could potentially cause memory exhaustion. indeni will alert when the idle timeout appears too high.",
  metricName = "f5-virtualserver-tcp-profile-idle-timeout",
  threshold = 1800.0,
  thresholdDirection = ThresholdDirection.ABOVE,
  applicableMetricTag = "name",
  alertItemsHeader = "Affected Profiles",
  alertDescription = "Having very long TCP idle timeouts for virtual servers could make the load balancer keep too many connections open, which in turn could potentially cause memory exhaustion.\n\nThis alert was added per the request of <a target=\"_blank\" href=\"https://se.linkedin.com/in/patrik-jonsson-6527932\">Patrik Jonsson</a>.",
  alertItemDescriptionFormat = "The idle timeout used is %.0f",
  baseRemediationText = "Investigate why the high idle timeout is being used and lower it if possible.")()
