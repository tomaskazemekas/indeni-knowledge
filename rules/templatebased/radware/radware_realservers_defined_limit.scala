package com.indeni.server.rules.library.templatebased.radware

import com.indeni.server.rules.RuleContext
import com.indeni.server.rules.library.NearingCapacityTemplateRule

/**
  *
  */
case class radware_realservers_defined_limit(context: RuleContext) extends NearingCapacityTemplateRule(context,
  ruleName = "radware_realservers_defined_limit",
  ruleFriendlyName = "Radware Alteon: Real servers defined limit approaching",
  ruleDescription = "Alteon devices allow for the configuration of multiple real servers, up to a limit. indeni will alert prior to configuration reaching the limit.",
  usageMetricName = "real-servers-usage",
  limitMetricName = "real-servers-limit",
  threshold = 80.0,
  alertDescriptionFormat = "There are %.0f real servers defined where the limit is %.0f.",
  baseRemediationText = "Consider removing certain real servers.")()
