#! META
name: junos-show-arp-no-resolve
description: JUNOS get ARP table information 
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
arp-total-entries:
    skip-documentation: true
arp-table:
    why: |
        The ARP table stores the mapping between IPs and MAC addresses to minimize the ARP traffic. The size of the ARP table and the incorrect mapping between IPs and MAC addreses can create many different issues for the network. So it is critical to monitor it.
    how: |
        This script retrieves the information from the ARP table via SSH connection to the device by running the command  "show arp no-resolve" command. Then it extracts IPs, MAC addresses, interfaces and status. It also shows the total entries in the ARP table. 
    without-indeni: |
        An administrator could log on to the device to run the command "show arp no-resolve" to collect the same information.
    can-with-snmp: true 
    can-with-syslog: false
    vendor-provided-management: |
        The commamnd line is available to retrieve this information

#! REMOTE::SSH
show arp no-resolve | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply//arp-table-information[1]
_metrics:
    -
        _groups:
            ${root}/arp-table-entry:
                _tags:
                    "im.name":
                        _constant: "arp-table"
                    "live-config":
                        _constant: "true"
                    "display-name":
                        _constant: "ARP Table"
                _value.complex:
                    target_ip:
                        _text: ip-address
                    interface:
                        _text: interface-name
                    mac:
                        _text: mac-address
                _temp:
                    mac_address:
                        _text: "mac-address"
        _transform:
            _value.complex:
                "success": |
                    {
                        if("${temp.mac_address}" ~ /Incomplete/) {
                           arp_success = "0"
                        } else {
                           arp_success = "1"
                        }
                        print arp_success
                    }         
        _value: complex-array
    -
        _tags:
            "im.name":
                _constant: "arp-total-entries"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "ARP - Total Entries"
            "im.dstype.displayType":
                _constant: "number"
        _value.double:
            _text: ${root}/arp-entry-count
