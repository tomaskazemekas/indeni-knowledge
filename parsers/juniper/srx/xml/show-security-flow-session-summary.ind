#! META
name: junos-show-security-flow-session-summary
description: JUNOS show security flow information
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: juniper
    os.name: junos
    product: firewall

#! COMMENTS
concurrent-connections-limit:
    why: |
        Each device has a limit to the number of concurrent sessions or connections it supports. Across the Juniper JUNOS product line, different devices are sized for different amounts of sessions. Reaching the maximum number of sessions allowed may result in an outage.
    how: |
       This script logs into the Juniper JUNOS-based device using SSH and retrieves the output of the "show security flow session summary" command. The output includes the session summary details for the device.
    without-indeni: |
        The session utilization is available through SNMP.
    can-with-snmp: true
    can-with-syslog: true
concurrent-connections:
    skip-documentation: true

#! REMOTE::SSH
show security flow session summary | display xml

#! PARSER::XML
_vars:
    root: /rpc-reply/flow-session-summary-information
_metrics:
    -
        _tags:
            "im.name":
                _constant: "concurrent-connections"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Sessions - Current"
            "im.dstype.displayType":
                _constant: "number"
        _value.double:
            _text: ${root}/active-sessions
    -
        _tags:
            "im.name":
                _constant: "concurrent-connections-limit"
            "live-config":
                _constant: "true"
            "display-name":
                _constant: "Sessions - Limit"
            "im.dstype.displayType":
                _constant: "number"
        _value.double:
            _text: ${root}/max-sessions

