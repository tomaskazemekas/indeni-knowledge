#! META
name: freebsd-os-netstat_rn
description: Get routing table using "netstat -rn"
type: monitoring
monitoring_interval: 1 minute
requires:
    freebsd-based: true

#! COMMENTS
static-routing-table:
    why: |
        It is important that the routing is configured the same for all cluster members of the same cluster. Otherwise there can be downtime in the event of a failover.
    how: |
        By running the command "netstat -rn" the routes are retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Listing static routes is only available from the command line interface or via SNMP.

connected-networks-table:
    why: |
        It is important that the connected interfaces is configured the same, for all cluster members of the same cluster. Otherwise there can be downtime in the event of a failure.
    how: |
          By running the command "netstat -rn" the routes are retrieved.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        Listing routes for directly connected interfaces is only available from the command line interface, or SNMP.

#! REMOTE::SSH
${nice-path} -n 15 netstat -rn

#! PARSER::AWK

# Destination        Type  Ref NextHop            Type  Ref Index    Interface
/Destination.*Type/ {
	# The list of columns has "Type" twice. So we cheat and replace the first one.
	line = $0
	sub(/Type/, "DestType", line) # Only replace the first instance of Type
	getColumns(trim(line), "[ \t]+", columns)
}

#################################
# The DestType's:
#     user  Installed by routing protocol process
#     kern  Installed by kernel
#     perm  Installed by kernel when routing table is initialized
#     dest  Directly reachable through an interface
#     clon  A clone route
#     cach  A cache maker route
#     unkn  Route unknown
#
# The type of the nexthop:
#     dscd  Causes packets to be dropped rather than forward but network
#           unreachable not generated
#     rjct  Causes packets to be dropped rather than forward and network
#           unreachable are sent to the packet originators
#     locl  Nexthop is for local addresses. Packets are sent to the local
#           protocl stack.
#     dest  Nexthop address corresponds to a directly connected neighbour.
#     recv  Nexthop type is currently unused
#     mcst  Nexthop is a multicast packet distribution
#     mgrp  Nexthop for packets sent to multicast groups
#     ulst  Nexthop is a set of unicast next-hops
#     rslv  Nexthop is resolving or is resolved in user space
#     tunl  Nexthop is a tunnel overlay
#     hold  Nexthop is waiting to be resolved into unicast or multicast type
#     intf  Nexthop directly to interface
#################################

# default            user    0 172.16.20.1        dest    3    65        eth2c0
# 0.0.0.0            perm    0                    locl    2    41
# 172.16.16/24       dest    0                    rslv    1    61        eth1c0
# 172.16.16.30       dest    0 172.16.16.30       locl    1    62        eth1c0
# 224/4              perm    0                    rjct    3    44
/(user|kern|perm|dest|clon|cach|unkn)/ {
	shorthandDestination = getColData(trim($0), columns, "Destination")

	if (shorthandDestination ~ /\//) {
		split(shorthandDestination, destinationParts, "/")
		destination = destinationParts[1]
		subnetprefix = destinationParts[2]

		# FreeBSD's netstat shortens the destination, we need to pad it:
		if (subnetprefix <= 8) {
			destination = destination ".0.0.0"
		} else if (subnetprefix <= 16) {
			destination = destination ".0.0"
		} else if (subnetprefix <= 24) {
			destination = destination ".0"
		}
	} else if (shorthandDestination == "default") {
		destination = "0.0.0.0"
		subnetprefix = 0
	} else {
		# No subnet in destination means it's /32
		destination = shorthandDestination
		subnetprefix = 32
	}

	destType = getColData(trim($0), columns, "DestType")
	nexthopType = getColData(trim($0), columns, "Type")

	# If its a direct connected network route
	if (destType == "dest") {
		iDirectRoute++
		
		directRoutes[iDirectRoute, "network"] = destination
		directRoutes[iDirectRoute, "mask"] = subnetprefix
	}

	# If its not a directly connected network
	if (destType != "dest") {
		iStaticRoute++

		gateway = getColData(trim($0), columns, "NextHop")
		
		# The gateway should be an IP address (v4 or v6). If it's not, it means the NextHop is empty and we
		# actually grabbed a different column.
		if ((gateway ~ /\./ || gateway ~ /\:/) && (subnetprefix < 32)) {
			staticRoutes[iStaticRoute, "network"] = destination
			staticRoutes[iStaticRoute, "mask"] = subnetprefix
			staticRoutes[iStaticRoute, "next-hop"] = gateway
		}
	}
}

END {
	writeComplexMetricObjectArrayWithLiveConfig("static-routing-table", null, staticRoutes, "Static routes")
	writeComplexMetricObjectArrayWithLiveConfig("connected-networks-table", null, directRoutes, "Directly Connected Networks")
}