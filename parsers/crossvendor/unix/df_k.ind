#! META
name: unix-df_k
description: run "df -k"
type: monitoring
monitoring_interval: 10 minutes
requires:
    or:
        -
            linux-based: true
        -
            linux-busybox: true
        -
            freebsd-based: true
    asg:
        neq: true

#! COMMENTS
disk-usage-percentage:
    why: |
        It is very important to monitor the disk space usage of a system. If the disk space is full it will prevent writing more data to the disk. Compressing and moving data from a disk that is 100% full is time consuming, which is why it is important to take care of any such issue early.
    how: |
        Using the built-in "df" command, the size and usage of all partitions are retrieved.
    without-indeni: |
        An administrator could login and manually list the disk space usage. Vendors generally provide tools which provide access to this information.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is accessible from the command line interface or vendor-provided tools, as well as SNMP.

disk-used-kbytes:
    why: |
        Used to display how much, in kilobytes, of the partition being used. If the file system gets data that should be written to disk can be lost.
    how: |
        Using the built in command "df"
    without-indeni: |
        An administrator could login and manually list the disk space usage. Vendors generally provide tools which provide access to this information.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is accessible from the command line interface or vendor-provided tools, as well as SNMP.

disk-total-kbytes:
    why: |
        Used to display the total partition size, in kilobytes.
    how: |
        Using the built in command "df"
    without-indeni: |
        An administrator could login and manually list the disk space usage. Vendors generally provide tools which provide access to this information.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: |
        This is accessible from the command line interface or vendor-provided tools, as well as SNMP.

#! REMOTE::SSH
${nice-path} -n 15 df -k

#! PARSER::AWK

############
# Script explanation: 
# The mount point info could be on a single line, or two, depending on the filesystem's name:
# But we don't care about the filesystem, so we can ignore that.
############


# 10157368   6384800   3248280  67% /
# /dev/sda1  295561     24017    256284   9% /boot
/(\d+)%/ {
    mount = $NF
    usage = $(NF-1)
    sub(/%/, "", usage)
    available = $(NF-2)
    used = $(NF-3)
	total = $(NF-4)

    mounttags["file-system"] = mount

    writeDoubleMetricWithLiveConfig("disk-usage-percentage", mounttags, "gauge", "60", usage, "Mount Points - Usage", "percentage", "file-system")
	writeDoubleMetricWithLiveConfig("disk-used-kbytes", mounttags, "gauge", "60", used, "Mount Points - Used", "kbytes", "file-system")
	writeDoubleMetricWithLiveConfig("disk-total-kbytes", mounttags, "gauge", "60", total, "Mount Points - Total", "kbytes", "file-system")
}
