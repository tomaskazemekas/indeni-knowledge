#! META
name: panos-show-system-state
description: fetch state information
type: monitoring
monitoring_interval: 10 minutes
requires:
    vendor: paloaltonetworks
    os.name: panos
    product: firewall

#! COMMENTS
routes-limit:
    why: |
        The routing table on a device requires a considerable amount of memory and so is limited in size. If the limit is hit, some routes may be missing and service disruption may occur.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the state table. In there it looks for the maximum number of routes allowed.
    without-indeni: |
        An administrator would be required to write a script. Alternatively, once an outage occurs, the administrator may see that the routing table is too big.
    can-with-snmp: false
    can-with-syslog: false
license-elements-limit:
    why: |
        Certain features have a license limitation on a Palo Alto Networks firewall. One such feature is the "vsys" feature ( https://www.paloaltonetworks.com/documentation/70/pan-os/pan-os/virtual-systems/virtual-systems-overview ). Knowing when the limitation on number of possible vsys's is nearing would help avoid issues during a maintenance.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the state table. In there it looks for the maximum number of vsys's allowed.
    without-indeni: |
        An administrator would be required to write a script. Alternatively, when creating a new vsys after the limit was reached, the web interface will provide an error message.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show system state

#! PARSER::AWK

function parseValue(_val) {
	if (_val ~ /0x/) {
		_h=tolower(_val);
		sub(/^0x/,"", _h)
        _v = 0
  		for(_i=1;_i<=length(_h);++_i){
    		_x=index("0123456789abcdef",substr(_h,_i,1))
    		if (!_x) return "NaN"
    		_v=(16*_v)+_x-1
  		}
  		return _v
	} else {
		return _val
	}
}

BEGIN {
}

# cfg.general.max-route: 0x3e8 
/cfg.general.max-route\:/ {
    value = parseValue($NF)
    if (value > 0) {
        writeDoubleMetricWithLiveConfig("routes-limit", null, "gauge", "600", value, "Routes - Limit", "number", "")
    }
}

# cfg.general.licensed-vsys: 0x1
/cfg.general.licensed-vsys/ {
	licensetags["name"] = "vsys"
    value = parseValue($NF)
    if (value > 0) {
        writeDoubleMetricWithLiveConfig("license-elements-limit", licensetags, "gauge", "600", value, "Licenses - Limit", "number", "name")
    }
}
