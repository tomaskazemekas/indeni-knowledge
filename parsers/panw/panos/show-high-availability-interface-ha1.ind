#! META
name: panos-show-high-availability-interface-ha1
description: fetch HA1 interface stats
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: paloaltonetworks
    os.name: panos
    "high-availability": true

#! COMMENTS
network-interface-admin-state:
    why: |
        If a network interface is set to be up (what's known as "admin up") but is actually down (a cable is not connected, the device on the other side is down, etc.) it is important to know.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for interfaces that are set to be up, but are actually down.
    without-indeni: |
        The status of network interfaces can be retrieved through SNMP.
    can-with-snmp: true
    can-with-syslog: true
network-interface-speed:
    why: |
        Generally, these days network interfaces are set at 1Gbps or more. Sometimes, due to a configuration or device issue, an interface can be set below that (to 100mbps or even 10mbps). As that is usually _not_ the intended behavior, it is important to track the speed of all network interfaces.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the actual runtime speed of each interface.
    without-indeni: |
        The speed of network interfaces can be retrieved through SNMP.
    can-with-snmp: true
    can-with-syslog: true
network-interface-duplex:
    why: |
        Generally, these days network interfaces are set at full duplex. Sometimes, due to a configuration or device issue, an interface can be set to half duplex. As that is usually _not_ the intended behavior, it is important to track the duplex setting of all network interfaces.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the actual runtime duplex of each interface.
    without-indeni: |
        The duplex of network interfaces can be retrieved through SNMP.
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-packets:
    why: |
        Tracking the number of packets flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of packets transmitted through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of bytes transmitted through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-rx-packets:
    why: |
        Tracking the number of packets flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of packets received through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-rx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of bytes received through the interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-rx-dropped:
    why: |
        If incoming packets are being dropped on a network interface, it is important to be aware of it. This may be due to a high load on the firewall, or another capacity issue.
    how: |
        This script logs into the Palo Alto Networks firewall through SSH and retrieves the status of all network interfaces. In that output, it looks for the number of packets dropped on an interface.
    without-indeni: |
        The traffic statistics of network interfaces can be retrieved through SNMP in newer versions of PAN-OS (7.x).
    can-with-snmp: true
    can-with-syslog: true
network-interface-state:
    skip-documentation: true
network-interface-mac:
    skip-documentation: true
network-interface-rx-errors:
    skip-documentation: true
network-interfaces:
    skip-documentation: true

#! REMOTE::HTTP
url: /api?type=op&cmd=<show><high-availability><interface>ha1</interface></high-availability></show>&key=${api-key}
protocol: HTTPS

#! PARSER::XML
_vars:
    root: /response/result/entry
_metrics:
    -
        _temp:
            state:
                _text: "${root}/info/state"
        _tags:
            name:
                _text: "${root}/info/name"
            im.name:
                _constant: "network-interface-state"
            high-availability-interface:
                _constant: "true"
        _transform:
            _value.double: |
                {
                    if ("${temp.state}" == "up") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -
        _temp:
            state_c:
                _text: "${root}/info/state_c"
        _tags:
            name:
                _text: "${root}/info/name"
            im.name:
                _constant: "network-interface-admin-state"
            high-availability-interface:
                _constant: "true"
        _transform:
            _value.double: |
                {
                    if ("${temp.state_c}" == "up" || "${temp.state_c}" == "auto") {
                        print "1"
                    } else {
                        print "0"
                    }
                }
    -
        _value.complex:
            value:
                _text: "${root}/info[not(duplex = 'unknown')]/duplex"
        _tags:
            "im.name":
                _constant: "network-interface-duplex"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
    -
        _temp:
            speed:
                _text: ${root}/info[not(speed = 'unknown')]/speed
        _tags:
            "im.name":
                _constant: "network-interface-speed"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
        _transform:
            _value.complex:
                value: |
                    {
                        print "${temp.speed}M"
                    }
    -
        _value.complex:
            value:
                _text: "${root}/info/hwaddr"
        _tags:
            "im.name":
                _constant: "network-interface-mac"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
    -
        _value.double:
            _text: "${root}/counters/tx-bytes"
        _tags:
            "im.name":
                _constant: "network-interface-tx-bytes"
            name:
                _text: "${root}/info/name"
            "im.dsType": 
                _constant: "counter"
    -
        _value.double:
            _text: "${root}/counters/rx-bytes"
        _tags:
            "im.name":
                _constant: "network-interface-rx-bytes"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
            "im.dsType": 
                _constant: "counter"
    -
        _value.double:
            _text: "${root}/counters/tx-packets"
        _tags:
            "im.name":
                _constant: "network-interface-tx-packets"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
            "im.dsType": 
                _constant: "counter"
    -
        _value.double:
            _text: "${root}/counters/rx-packets"
        _tags:
            "im.name":
                _constant: "network-interface-rx-packets"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
            "im.dsType": 
                _constant: "counter"
    -
        _value.double:
            _text: "${root}/counters/rx-errs"
        _tags:
            "im.name":
                _constant: "network-interface-rx-errors"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
            "im.dsType": 
                _constant: "counter"
    -
        _value.double:
            _text: "${root}/counters/rx-drop"
        _tags:
            "im.name":
                _constant: "network-interface-rx-dropped"
            name:
                _text: "${root}/info/name"
            high-availability-interface:
                _constant: "true"
            "im.dsType": 
                _constant: "counter"
    -
        _value.complex:
            name:
                _text: "${root}/info/name" 
        _tags:
            "im.name":
                _constant: "network-interfaces"
            high-availability-interface:
                _constant: "true"
        _value: complex-array
