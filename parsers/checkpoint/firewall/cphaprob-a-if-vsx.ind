#! META
name: cphaprob_a_if_vsx
description: run "cphaprob -a if" on VSX
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    high-availability: "true"
    vsx: "true"
    role-firewall: true

#! COMMENTS
cphaprob-required-interfaces:
    skip-documentation: true

cphaprob-required-secured-interfaces:
    skip-documentation: true

cphaprob-up-interfaces:
    skip-documentation: true

cluster-vip:
    skip-documentation: true

clusterxl-ccp-mode:
    skip-documentation: true

cphaprob-up-secured-interfaces:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw vsx stat -l && ${nice-path} -n 15 cphaprob -a if

#! PARSER::AWK

function dumpFoundInterfaces() {
	# Do not write metrics unless cluster services are started.
	if (servicesStarted != 0) {
		writeDoubleMetric("cphaprob-up-interfaces", vstags, "gauge", "60", foundInterfaces)
		writeDoubleMetric("cphaprob-up-secured-interfaces", vstags, "gauge", "60", foundSecuredInterfaces)
		writeComplexMetricObjectArray("cluster-vip", vstags, clustervip)
	}
}

BEGIN {
	foundInterfaces = 0
	foundSecuredInterfaces = 0
}

# VSID:            0
/VSID:/ {
	vsid = trim($NF)
}

# Name:            VSX-CXL2-Gear
/Name:/ {
	vsname = trim($NF)
	vsnameArr[vsid] = vsname
}

#vsid 0:
/^vsid/ {
    if (vsid != "") {
        dumpFoundInterfaces()
    }
    vsid=trim($NF)
    vsid = substr(vsid, 1, length(vsid) - 1)
	vstags["vs.id"] = vsid
	vstags["vs.name"] = vsnameArr[vsid]
	foundInterfaces = 0
	foundSecuredInterfaces = 0
	servicesStarted = 1
}

#HA module not started.
/HA module not started./ {
	servicesStarted = 0
}

#Required interfaces: 2
/Required interfaces/ {
	writeDoubleMetric("cphaprob-required-interfaces", vstags, "gauge", "60", $NF)
}

#Required secured interfaces: 1
/Required secured interfaces/ {
	writeDoubleMetric("cphaprob-required-secured-interfaces", vstags, "gauge", "60", $NF)
}

# Match UP secured and non-secured interfaces
#eth2       UP                    sync(secured), multicast
/UP.*secured/ {
    if ($0 !~ /DOWN/) {
        foundInterfaces = foundInterfaces + 1
    }
}

#eth2       UP                    sync(secured), multicast
/UP.*\(secured/ {
    if ($0 !~ /DOWN/) {
        foundSecuredInterfaces = foundSecuredInterfaces + 1
    }
}

# eth1            1.1.1.1
/(?:[0-9]{1,3}\.){3}[0-9]{1,3}$/ {
	iint++
	clustervip[iint, $1]=$2
}

END {
    if (vsid != "") {
        dumpFoundInterfaces()
    }
}
