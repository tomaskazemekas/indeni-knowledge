#! META
name: chkp-fw-ctl-pstat-novsx
description: run "fw ctl pstat" on non-vsx or VS0
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: checkpoint
    vsx:
        neq: true
    role-firewall: true
    asg:
        neq: true

#! COMMENTS
kernel-memory-usage:
    why: |
        If the firewall kernel memory becomes fully utilized, performance may be impacted and traffic may be dropped. It is critical to monitor the kernel memory's usage and handle the issue prior to full utilization.
    how: |
        indeni uses the built-in Check Point "fw ctl pstat" command to retreive the usage of the kernel memory.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the kernel memory is only available from the command line interface.

chkp-agressive-aging:
    why: |
        If the memory utilization reaches a predetermined threshold, a feature called "aggressive aging" could be automaticly enabled. This feature starts removing old TCP connections ahead of the planned expiration time. This can cause performance and traffic flow issues for applications that cannot handle this. Therefore it is very useful to know when this feature has become active.
    how: |
        indeni uses the built-in Check Point "fw ctl pstat" command to retreive the status of the "agressive aging" feature.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        Listing the kernel memory is only available from the command line interface.


concurrent-connections:
    why: |
        It is possible to set a limit on how many connections a device can support. If the limit is reached, no new connections are allowed and this usually results in traffic loss.
    how: |
        indeni uses the built-in Check Point "fw ctl pstat" command to retreive the current usage an limit.
    without-indeni: |
        An administrator could login and manually run the command.
    can-with-snmp: false
    can-with-syslog: false
    vendor-provided-management: |
        The current and historical number of concurrent connections is accessible through Check Point SmartView Monitor.

concurrent-connections-limit:
    skip-documentation: true

#! REMOTE::SSH
${nice-path} -n 15 fw ctl pstat

#! PARSER::AWK

BEGIN {
    devicename=""
}

# Memory used: 20% (871 MB out of 4163 MB) - below watermark
/Memory used.*out of.*/ {
    usage=substr($3, 1, length($3)-1)
    writeDoubleMetricWithLiveConfig("kernel-memory-usage", null, "gauge", "60", usage, "Kernel Memory", "percentage", "")
}

# Aggressive Aging is not active
/Aggressive Aging/ {
	message=trim($0)
	if (message == "Aggressive Aging is not active") {
		writeDoubleMetric("chkp-agressive-aging", null, "gauge", 60, 0)
	} else if (message == "Aggressive Aging is active") {
		writeDoubleMetric("chkp-agressive-aging", null, "gauge", 60, 1)
	}
}

# Concurrent Connections: 13 (Unlimited)
# Concurrent Connections: 0% (5 out of 24900) - below watermark
/Concurrent Connections:/ {
	if ($NF == "\(Unlimited\)") {
		connections = $3
	} else {
		connections = $4
		gsub(/\(/,"",connections)
		
		connectionLimit = $7
		gsub(/\)/,"",connectionLimit)
	}
	
	writeDoubleMetric("concurrent-connections", null, "gauge", 60, connections)
	if (connectionLimit) {
		writeDoubleMetric("concurrent-connections-limit", null, "gauge", 60, connectionLimit)
	}
}
