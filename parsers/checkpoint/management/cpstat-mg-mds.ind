#! META
name: cpmds-cpstat-mg
description: Shows status of all MDS in management server
type: monitoring
monitoring_interval: 5 minutes
requires:
    vendor: checkpoint
    os.name: gaia
    role-management: true
    vsx: true
    mds: true

#! COMMENTS
mgmt-status:
    skip-documentation: true

#! REMOTE::SSH
COLUMNS=150 && export COLUMNS && ${nice-path} -n 15 mdsstat |grep CMA | awk '{gsub(/\|/,"",$3); print $3}' | while read name; do mdsenv $name && ${nice-path} -n 15 mdsstat $name && ${nice-path} -n 15 cpstat mg; done


#! PARSER::AWK

############
# ToDo: Add the status message to a descriptive tag in the future
###########

function addVsTags(tags) {
	tags["vs.ip"] = vsIp
    tags["vs.name"] = vsName
}

function dumpStatus() {
	addVsTags(t)
	if ( is_started == 1 && active_status == "active" && status == "OK" ) {
		writeDoubleMetricWithLiveConfig("mgmt-status", t, "gauge", "300", "1", "Management Services Status", "state", "vs.name")
	} else {
		writeDoubleMetricWithLiveConfig("mgmt-status", t, "gauge", "300", "0", "Management Services Status", "state", "vs.name")
	}
	
	# clear tags and variables
	delete t
	is_started=""
	active_status=""
	status=""
}


# | CMA |MDM-VSX_Management_Server | 10.10.6.14      | up 1531    | up 1616  | up 1493  | up 1720  |
/^\| CMA \|/ {
 if (vsName != "") {
		# write the previous mds's data
		dumpStatus()
	}

	vsName=$3
	vsIp=$5
	
	# Remove starting "|"
	gsub(/\|/,"",vsName)
}

#Is started:    1
/Is started/ {
	split($0,splitArr,":")
	is_started=trim(splitArr[2])
}

# Active status: active
/Active status/ {
	split($0,splitArr,":")
	active_status = trim(splitArr[2])
}


# Status:        OK
/Status/ {
	split($0,splitArr,":")
	status = trim(splitArr[2])
}

END {
	dumpStatus()
}
