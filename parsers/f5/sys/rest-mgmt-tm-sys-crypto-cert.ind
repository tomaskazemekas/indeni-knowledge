#! META
name: f5-rest-mgmt-tm-sys-crypto-cert
description: Get expiration dates of certificates
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
certificate-expiration:
    why: |
        Expired certificate would present warnings to clients advising them not to proceed with connecting to the resource in question.
    how: |
        This alert uses the iControl REST interface to extract the certificates expiration dates.
    without-indeni: |
        Login to the device over SSH enter TMSH and issue this command: "run sys crypto check-cert verbose enabled". This will show when the certificates on the device will expire.
    can-with-snmp: true
    can-with-syslog: false
    vendor-provided-management: Unknown

#! REMOTE::HTTP
url: /mgmt/tm/sys/crypto/cert?$select=name,apiRawValues,commonName
protocol: HTTPS

#! PARSER::JSON

_metrics:
    - # This metric is for certificates that has the property commonName
        _groups:
            "$.items[?(@.commonName)]":
                _temp:
                    "expiration":
                        _value: "apiRawValues.expiration"
                _tags:
                    "im.name":
                        _constant: "certificate-expiration"
                    "im.dstype.displaytype":
                        _constant: "date"
                    "name":
                        _value: "name"
                    "commonName":
                        _value: "commonName"
        _transform:
            _value.double: |
                {
                    #Dec 15 23:59:59 2017 GMT
                    datestring = "${temp.expiration}"
                    gsub(/\s+/, " ", datestring)
                    split(datestring, dateArray, /\s/)

                    month = parseMonthThreeLetter(dateArray[1])
                    day = dateArray[2]
                    year = dateArray[4]

                    #Split the time
                    #23:59:59 
                    split(dateArray[3],timeArray,/:/)

                    #Get hour, minute, second
                    hour = timeArray[1]
                    minute = timeArray[2]
                    second = timeArray[3]

                    #Calculate seconds to epoch
                    secondsSinceEpoch = datetime(year, month, day, hour, minute, second)
                    print secondsSinceEpoch
                }
    - #This is for the certificates that does not have the property commonName
        _groups:
            "$.items[?(!@.commonName)]":
                _temp:
                    "expiration":
                        _value: "apiRawValues.expiration"
                _tags:
                    "im.name":
                        _constant: "certificate-expiration"
                    "im.dstype.displaytype":
                        _constant: "date"
                    "name":
                        _value: "name"
        _transform:
            _value.double: |
                {
                    #Dec 15 23:59:59 2017 GMT
                    datestring = "${temp.expiration}"
                    gsub(/\s+/, " ", datestring)
                    split(datestring, dateArray, /\s/)

                    month = parseMonthThreeLetter(dateArray[1])
                    day = dateArray[2]
                    year = dateArray[4]

                    #Split the time
                    #23:59:59 
                    split(dateArray[3],timeArray,/:/)

                    #Get hour, minute, second
                    hour = timeArray[1]
                    minute = timeArray[2]
                    second = timeArray[3]

                    #Calculate seconds to epoch
                    secondsSinceEpoch = datetime(year, month, day, hour, minute, second)

                    print secondsSinceEpoch
                }
