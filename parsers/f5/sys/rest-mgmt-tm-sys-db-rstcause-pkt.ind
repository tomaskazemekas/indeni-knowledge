#! META
name: f5-rest-mgmt-tm-sys-db-rstcause-pkt
description: Determine if the debug value rstcause.pkt is set
type: monitoring
monitoring_interval: 30 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
debug-status:
    why: |
        Setting the tm.rstcause.pkt flag would make the F5 device add additional troubleshooting in any RST packets it sends. This is useful when troubleshooting why RST's occur, but could advertise information about your environment to a potential attacker. Thus it is not good to leave this setting on for longer periods of time.
    how: |
        This alert logs into the F5 unit via iControl REST and retrieves the status of the tm.rstcause.pkt flag.
    without-indeni: |
        An administrator would be able to see if the flag is on by logging into the device through SSH, entering TMSH and issuing the command "list sys db tm.rstcause.pkt".
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::HTTP
url: /mgmt/tm/sys/db/tm.rstcause.pkt?$select=value
protocol: HTTPS

#! PARSER::JSON

_metrics:
    -
        _tags:
            "im.name":
                _constant: "debug-status"
            "name":
                _constant: "tm.rstcause.pkt"
            "im.dstype.displaytype":
                _constant: "state"
        _temp:
            "rstValue":
                _value: "$.value"
        _transform:
            _value.double: |
                {
                    if("${temp.rstValue}" == "disable"){
                        print "0"
                    } else {
                        print "1"
                    }
                }
