 #! META
name: f5-rest-net-interface-stats
description: Determine network interface metrics
type: monitoring
monitoring_interval: 1 minute
requires:
    vendor: "f5"
    product: "load-balancer"
    rest-api: "true"
    shell: "bash"

#! COMMENTS
network-interface-state:
    why: |
        Interfaces in the "down" state could result in downtime or reduced redundancy.
    how: |
        The state of the interface is retrieved via the F5 iControl REST API.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces to see the status of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-admin-state:
    why: |
        An administrator might set a network interface to be disabled for troubleshooting, but. Should he he forget about doing this network trunks might be running at reduced capacity.
    how: |
        This alert uses the F5 iControl REST API to retrieve the status of all physical network interfaces. In that output, it looks for interfaces that are set to be up, but are actually down.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces to see the admin status of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-mac:
    why: |
        To be able to search for MAC addresses in indeni, this data needs to be stored.
    how: |
        This alert uses the F5 iControl REST API to retrieve the MAC addresses of all physical network interfaces.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces to see the mac address of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-mtu:
    why: |
        The MTU sometimes needs to be adjusted. Storing this gives an administrator an easy way to view the MTU from a large number of devices, as well as identifying incorrectly set MTU.
    how: |
        This alert uses the F5 iControl REST API to retrieve the MTU from all physical interfaces.
    without-indeni: |
        An administrator could log into the device through SSH, entering TMSH and executing the command "show net interface all-properties" to see the MTU for each interface.
    can-with-snmp: true
    can-with-syslog: false
network-interface-rx-packets:
    why: |
        Tracking the number of packets flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets received through the interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-tx-packets:
    why: |
        Tracking the number of packets flowing through each VLAN interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets transmitted through the interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-rx-dropped:
    why: |
        If incoming packets are being dropped on a network interface, it is important to be aware of it. This may be due to a high load on the unit, or another capacity issue.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets dropped on an interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: true
network-interface-tx-dropped:
    why: |
        If outgoing packets are being dropped on a network interface, it is important to be aware of it. This may be due to a high load on the unit, or another capacity issue.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of packets dropped on an interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-rx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of bytes received through the interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-tx-bytes:
    why: |
        Tracking the amount of data flowing through each network interface is important to identify potential issues, spikes in traffic, etc.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of bytes transmitted through the interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-tx-errors:
    why: |
        Transmit errors on an interface could indicate a problem.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of transmit errors on each interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-rx-errors:
    why: |
        Receive errors on an interface could indicate a problem.
    how: |
        This alert logs into the F5 unit through the F5 iControl REST API and retrieves the metrics from all network interfaces. In that output, it looks for the number of receive errors on each interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces -> Statistics to see the statistics of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties".
    can-with-snmp: true
    can-with-syslog: false
network-interface-speed:
    why: |
        Generally, these days network interfaces are set at 1Gbps or more. Sometimes, due to a configuration or device issue, an interface can be set below that (to 100mbps or even 10mbps). As that is usually _not_ the intended behavior, it is important to track the speed of all network interfaces.
    how: |
        This alert logs into the F5 unit through the iControl REST API and retrieves the status of all network interfaces. In that output, it looks for the actual runtime speed of each interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces to see the speed of each interface by looking at the column "Media Speed". This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties" and looking at the column "Media". The numbers are the value of the speed in "MegaBits". Example: 1000T-FD means "1000 Mbit".
    can-with-snmp: true
    can-with-syslog: false
network-interface-duplex:
    why: |
        Generally, these days network interfaces are set at full duplex. Sometimes, due to a configuration or device issue, an interface can be set to half duplex. As that is usually _not_ the intended behavior, it is important to track the duplex setting of all network interfaces.
    how: |
        This alert logs into the F5 unit through the iControl REST API and retrieves the status of all network interfaces. In that output, it looks for the actual runtime duplex of each interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces and click on each interface in the list to see the duplex of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties" and looking at the column "Media". A value of "FD" means "Full Duplex", while a value of "HD" means "Half Duplex". Example: 1000T-FD means "Speed: 1000 Mbit, Duplex: Full, Media Type: Copper Twister Pair".
    can-with-snmp: true
    can-with-syslog: false
network-interface-type:
    why: |
        The type of interface can be useful for administrators.
    how: |
        This alert logs into the F5 unit through the iControl REST API and retrieves the status of all network interfaces. In that output, it looks for the actual runtime duplex of each interface.
    without-indeni: |
        An administrator could login to the web interface of the device and go to Network -> Interfaces and click on each interface in the list to see the media type of each interface. This information is also available by logging into the device through SSH, entering TMSH and executing the command "show net interface all-properties" and looking at the column "Media". Example: "1000T-FD" means "Speed: 1000 Mbit, Duplex: Full, Media Type: Copper Twister Pair".
    can-with-snmp: true
    can-with-syslog: false
network-interface-admin-state:
    why: |
        If a network interface is set to be up (what's known as "admin up") but is actually down (a cable is not connected, the device on the other side is down, etc.) it is important to know.
    how: |
        This alert logs into the F5 device through SSH and retrieves the status of all network interfaces. In that output, it looks for interfaces that are set to be up, but are actually down.
    without-indeni: |
        An administrator could verify that interface admin state by logging into the device through SSH, entering TMSH and issuing the command "show net interface".
    can-with-snmp: true
    can-with-syslog: true

#! REMOTE::HTTP
url: /mgmt/tm/net/interface/stats?options=all-properties&$select=tmName,status,macAddress,mtu,counters.bitsIn,counters.bitsOut,counters.pktsIn,counters.pktsOut,counters.dropsIn,counters.dropsOut,counters.errorsIn,counters.errorsOut,mediaActive
protocol: HTTPS

#! PARSER::JSON

_metrics:
    
    ##########################################
    #   Interface state
    ##########################################
    
    - #Get interface state for interfaces that is up
        _groups:
            "$.entries.*.nestedStats.entries[?(@.status.description == 'up')]":
                _tags:
                    "im.name":
                        _constant: "network-interface-state"
                    "im.dstype.displaytype":
                        _constant: "state"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _constant: "1"
                    
    - #Get interface state for interfaces that is not up, disabled, unpopulated or uninitialized (we won't write metrics for disabled, unitialized or missing interfaces)
        _groups:
            "$.entries.*.nestedStats.entries[?(@.status.description != 'up' && @.status.description != 'disabled' && @.status.description != 'uninit' && @.status.description != 'miss' )]":
                _tags:
                    "im.name":
                        _constant: "network-interface-state"
                    "im.dstype.displaytype":
                        _constant: "state"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _constant: "0"

                    
    ##########################################
    #   Interface admin-state
    ##########################################
    - #Get admin state for interfaces that is up
        _groups:
            "$.entries.*.nestedStats.entries[?(@.status.description != 'disabled')]":
                _tags:
                    "im.name":
                        _constant: "network-interface-admin-state"
                    "im.dstype.displaytype":
                        _constant: "state"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _constant: "1"
                    
    - #Get interface admin-state for interfaces that is disabled
        _groups:
            "$.entries.*.nestedStats.entries[?(@.status.description == 'disabled')]":
                _tags:
                    "im.name":
                        _constant: "network-interface-admin-state"
                    "im.dstype.displaytype":
                        _constant: "state"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _constant: "0"
                    
    ##########################################
    #   Interface mac-address
    ##########################################
    
    - #Get interface mac-address
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-mac"
                    "name":
                        _value: "tmName.description"
                _value.complex:
                    value:
                        #  "value" : "02:01:23:45:00:09"
                        _value: "macAddress.description"
                    
    ##########################################
    #   Interface mtu
    ##########################################
    
    - #Get interface mtu
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-mtu"
                    "name":
                        _value: "tmName.description"
                _value.complex:
                    value:
                        #  "value" : "8096"
                        _value: "mtu.value"
                        
    ##########################################
    #   Interface statistics
    ##########################################
    
    - #Get interface rx-bytes
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-bytes"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "bytes"
                    "name":
                        _value: "tmName.description"
                _temp:
                    "bitsIn":
                        _value: "['counters.bitsIn'].value"
        _transform:
            _value.double: |
                {
                    #Convert to bytes
                    print "${temp.bitsIn}"/8
                }
    - #Get interface tx-bytes
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-bytes"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "bytes"
                    "name":
                        _value: "tmName.description"
                _temp:
                    "bitsOut":
                        _value: "['counters.bitsOut'].value"
        _transform:
            _value.double: |
                {
                    #Convert to bytes
                    print "${temp.bitsOut}"/8
                }
    - #Get interface rx-packets
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-packets"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['counters.pktsIn'].value"
    - #Get interface tx-packets
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-packets"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['counters.pktsOut'].value" 
    - #Get interface rx-drops
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-drops"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['counters.dropsIn'].value" 
    - #Get interface tx-drops
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-drops"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['counters.dropsOut'].value" 
    - #Get interface rx-errors
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-rx-errors"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['counters.errorsIn'].value" 
    - #Get interface tx-errors
        _groups:
            "$.entries.*.nestedStats.entries":
                _tags:
                    "im.name":
                        _constant: "network-interface-tx-errors"
                    "im.dsType":
                        _constant: "counter"
                    "im.dstype.displaytype":
                        _constant: "number"
                    "name":
                        _value: "tmName.description"
                _value.double:
                    _value: "['counters.errorsOut'].value" 
    
    ##########################################
    #   Interface speed, duplex and type
    ##########################################
    
    - #Get interface speed
        _groups:
            "$.entries.*.nestedStats.entries[?(@.mediaActive.description != 'none' && @.tmName.description != 'mgmt')]":
                _tags:
                    "im.name":
                        _constant: "network-interface-speed"
                    "name":
                        _value: "tmName.description"
                _temp:
                    "media":
                        _value: "mediaActive.description" 
        _transform:
            _value.complex:
                value: |
                    {
                        media = "${temp.media}"
                        
                        #1000T-FD
                        split(media, mediaArray, /-/)
                        
                        interfaceSpeed = mediaArray[1]
                        
                        #1000T will be 1000
                        gsub(/[^0-9]+/, "",interfaceSpeed)
                        
                        interfaceSpeed = interfaceSpeed "M"
                        
                        #  "value" : "1000M"
                        print interfaceSpeed
                        
                    }
    - #Get interface duplex
        _groups:
            "$.entries.*.nestedStats.entries[?(@.mediaActive.description != 'none')]":
                _tags:
                    "im.name":
                        _constant: "network-interface-duplex"
                    "name":
                        _value: "tmName.description"
                _temp:
                    "media":
                        _value: "mediaActive.description" 
        _transform:
            _value.complex:
                value: |
                    {
                        media = "${temp.media}"
                        
                        #1000T-FD
                        split(media, mediaArray, /-/)

                        #Determine duplex settings
                        duplex = mediaArray[2]
                        
                        if(duplex == "FD"){
                            #  "value" : "full"
                            print "full"
                        } else if (duplex == "HD") {
                            #  "value" : "half"
                            print "half"
                        } else {
                            #  "value" : "unknown"
                            print "unknown"
                        }
                    }
    - #Get interface type
        _groups:
            "$.entries.*.nestedStats.entries[?(@.mediaActive.description != 'none')]":
                _tags:
                    "im.name":
                        _constant: "network-interface-type"
                    "name":
                        _value: "tmName.description"
                _temp:
                    "media":
                        _value: "mediaActive.description" 
        _transform:
            _value.complex:
                value: |
                    {
                        media = "${temp.media}"
                        
                        #1000T-FD
                        split(media, mediaArray, /-/)
                        
                        #Determine interface type
                        interfaceType = mediaArray[1]
                        
                        #1000T will be T
                        gsub(/[0-9]+/, "",interfaceType)
                        
                        if(interfaceType != ""){
                            #  "value" : "T"
                            print interfaceType
                        } else {
                            #  "value" : "unknown"
                            print "unknown"
                        }
                        
                    }
