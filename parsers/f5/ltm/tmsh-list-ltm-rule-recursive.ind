 #! META
name: f5-tmsh-list-ltm-rule-recursive
description: Find use of matchclass
type: monitoring
monitoring_interval: 60 minutes
requires:
    vendor: "f5"
    product: "load-balancer"
    linux-based: "true"
    shell: "bash"

#! COMMENTS
f5-matchclass-used:
    why: |
        The command "matchclass" is used to check if a value is contained within a data group list. While still supported the command has been deprecated in favor of the more powerful and efficient "class" command.
    how: |
        This alert logs into the F5 through SSH and parses all iRules looking for usage of the command "matchclass".
    without-indeni: |
        Log into the device through SSH. Enter TMSH and issue the command "cd /;list ltm rule recursive". Look through each iRule definition for the use of the "matchclass" command.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
tmsh -q -c "cd /;list ltm rule recursive"

#! PARSER::AWK

#ltm rule Common/samplematchclassrule {
/^ltm\srule/{

    ruleTags["name"] = "/" $3
    matchClassFound = 0
}

/^}$/{
    if(matchClassFound == 1){
        writeComplexMetricString("f5-matchclass-used", ruleTags, "true")
    } else {
        writeComplexMetricString("f5-matchclass-used", ruleTags, "false")
    }
}

{
    #Match all except the rule name row and the end of a rule definition
    if(!match($0, /^ltm\srule/ && !match($0, /^}$/))){
        if(match($0, /[^\#]*matchclass/)){
            matchClassFound = 1
        }
    }
}

