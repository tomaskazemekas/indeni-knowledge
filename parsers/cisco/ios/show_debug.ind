#! META
name: ios-show-debug
description: IOS show debug
type: monitoring
monitoring_interval: 5 minute
requires:
    vendor: cisco
    os.name: ios

#! COMMENTS
debug-status:
    why: |
        Enabling debugging on a Cisco IOS device enables the system administrator to get low level information about the system's operation. This functionality is often used for troubleshooting and it has a potential high impact on CPU utilization and system stability. It is highly undesirable to keep debugging enabled for extended periods of time.
    how: |
        This script logs into the Cisco IOS switch using SSH and retrieves the status of running debugs using the 'show debugging' CLI command. In normal operation there should not be any debugs enabled.
    without-indeni: |
        Enabled debug can be detected by logging to the device or by monitoring syslog message in case debug level logging has been enabled.
    can-with-snmp: false
    can-with-syslog: true

#! REMOTE::SSH
show debug

#! PARSER::AWK

#VRRP:                                                  <<< SAMPLE Router
#  VRRP Events debugging is on
#HSRP:
#  HSRP Events debugging is on
#GLBP:
#  GLBP Errors debugging is on
#  GLBP Events debugging is on
#General Ethernet:                                      <<< SAMPLE Switch
#  Ethernet network interface debugging is on
#IP-STATIC:
#  IP static routing detail debugging is on
#SSH:
#  SSH Client debugging is on
#fastethernet:
#  Fast Ethernet events debugging is on

BEGIN {
    entry = 0
}

/ debugging is/ {

    line = trim($0)
    fields = NR
    entry++
    split(line, p1, " debugging is")
    
    name=p1[1]
    state=p1[2]
    
    writeDebug("@ " NR ":" NF " debug of : ["  name "] with state [" state "]")   
    debug_tags["name"] = trim(name)
    writeDoubleMetric("debug-status", debug_tags, "gauge", 300, "1.0")
}

END {
}
