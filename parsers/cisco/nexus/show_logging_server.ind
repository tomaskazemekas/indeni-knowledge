#! META
name: nexus-show-logging-server
description: Nexus syslog servers
type: monitoring
monitoring_interval: 12 hours
requires:
    vendor: cisco
    os.name: nxos

#! COMMENTS
syslog-servers:
    why: |
       Collect the configured syslog servers and the event severity each one is configured the report. It is a best practice to have at least one and no more than three, syslog server configured. Syslog is used to store event logs on an external system and it is critical for detecting and analyzing network events. 
    how: |
       This script logs into the Cisco Nexus switch using SSH and retrieves the syslog servers list using the "show logging server" command.
    without-indeni: |
       An administrator will have to login to the device to validate the configured syslog servers.
    can-with-snmp: false
    can-with-syslog: false

#! REMOTE::SSH
show logging server

#! PARSER::AWK

BEGIN {
    i = 0
    server_ip = ""
    server_severiry = ""
}

# {1.1.1.1}
/\{[0-9a-f.:]+\}/ {
    server_ip = $0
    gsub(/\{/, "", server_ip)
    gsub(/\}/, "", server_ip)
    server_ip = trim(server_ip)
}

#         server severity:        errors
/server severity/ {
    split($0, r, ":")

    server_severity = trim(r[2])

    if ((server_severity == "") || (server_ip == "")) {
        next
    }

    writeDebug("Syslog server:" server_ip ", severity:" server_severity)
    i++
    syslog_list[i, "ipaddress"] = server_ip
    syslog_list[i, "severity"] = server_severity

    server_ip = ""
    server_severiry = ""
}

END {
    writeComplexMetricObjectArrayWithLiveConfig("syslog-servers", null, syslog_list, "Syslog Servers")
}
